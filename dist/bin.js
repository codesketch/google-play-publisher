#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var path_1 = require("path");
var commander_1 = require("commander");
var program = new commander_1.Command();
var index_1 = require("./index");
program
    .description("Publish Android App Bundle to Google Play")
    .requiredOption("-k, --keyFile <path>", "set google api json key file")
    .requiredOption("-p, --packageName <name>", "set package name (com.some.app)")
    .requiredOption("-a, --aabFile <path>", "set path to .aab file")
    .requiredOption("-t, --track <track>", "set track (production, beta, alpha...)")
    .option("-c, --changesNotSentForReview", "Set changesNotSentForReview flag")
    .option("-s, --status", "Set the status at the store")
    .option("-e, --exit", "exit on error with error code 1.")
    .parse(process.argv);
var options = program.opts();
(0, index_1.publish)({
    keyFile: (0, path_1.resolve)(options.keyFile),
    packageName: options.packageName,
    aabFile: (0, path_1.resolve)(options.aabFile),
    track: options.track,
    changesNotSentForReview: options.changesNotSentForReview,
    status: options.status || 'completed'
})
    .then(function () {
    console.log("Publish complete.");
})
    .catch(function (error) {
    console.error(error.message);
    process.exit(options.exit ? 1 : 0);
});
