interface SchemaPublish {
    keyFile: string;
    packageName: string;
    aabFile: string;
    track: string;
    changesNotSentForReview: boolean;
    status?: string;
}
export declare const publish: ({ keyFile, packageName, aabFile, track, changesNotSentForReview, status }: SchemaPublish) => Promise<void>;
export {};
